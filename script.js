const toggler = document.querySelector('.navbar-toggler')
const navbarCollapse = document.querySelector('.navbar-collapse')

toggler.addEventListener('click' , function() {
    toggler.classList.toggle('fa-rotate-180')
    navbarCollapse.classList.toggle('bg-light')
    toggler.classList.toggle('active')
})

document.addEventListener('scroll' , function() {
    const navbar = document.querySelector('.navbar')
    if(window.scrollY > 100) {
        navbar.classList.remove('navbar-light', 'bg-light')
        navbar.classList.add('navbar-dark', 'bg-dark')
        navbarCollapse.classList.add('bg-dark')
    } else {
        navbar.classList.add('navbar-light', 'bg-light')
        navbar.classList.remove('navbar-dark', 'bg-dark')
        navbarCollapse.classList.remove('bg-dark')
    }
})

fetch('./article.json').then((data) => data.json()).then((articles) => {
    
function populatelastItems(category, cardWrapper, moreWrapper) {
        
    let lastItems = articles.filter(article => article.category == category).slice(0,5)
    
    let itemCard = document.querySelector('#'+cardWrapper);
        
    if (!itemCard) {
        return;
    }

    lastItems.forEach(item => {
        let card = document.createElement('div');

        card.classList.add('col-6', 'col-md-2', 'my-5');

        card.innerHTML = 

        `
        <div class="card bg-card d-flex align-items-center">
            <img src="${item.img}" class="card-img-top img-fluid" alt="...">
            <button ad-id="${item.id}" class="btn btn-card">Quick view</button>
            <div class="card-body p-0 d-flex justify-content-center align-items-center flex-column">
                <h6 class="card-title fw-bold my-2">${item.name}</h6>
            <a shop-id="${item.id}" class="btn btn-shop">${isInCart(item.id)}</a>
                <p class="card-text text-center">${item.price.toFixed(2)} €</p>
            </div>
        </div>
        `

        itemCard.appendChild(card);

        card.querySelector('.btn-shop').addEventListener('click', updateCart)

    });

    let moreBtnsWrapper = document.querySelectorAll('#'+moreWrapper)

    moreBtnsWrapper.forEach(moreBtn => {
        moreBtn.addEventListener('click', function() {
            window.location.href="./article.html?cat="+category
        })
    })

    let btnCard = document.querySelectorAll('.btn-card')
                btnCard.forEach(btn => {
                    let articleId = btn.getAttribute('ad-id')
                    let article = articles.filter(art => art.id == articleId)
                    article = article[0]
                    btn.addEventListener('click', function() {
                        Swal.fire({
                            html:
                            `
                        <div class="row justify-content-around">
                            <div class="col-12 col-md-6">
                                <img src="${article.img}" class="card-img-top img-fluid" alt="...">
                            </div>
                                
                            <div class="col-12 col-md-6">
                                <h4>${article.name}</h4>
                                <h5>${article.brand}</h5>
                                <p>${article.price} €</p>
                                <p class="text-start">${article.description}</p>
                            </div>
                        </div>
                            `,
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Great!',
                            confirmButtonAriaLabel: 'Thumbs up, great!',
                            cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i>',
                            cancelButtonAriaLabel: 'Thumbs down',
                            background: '#fff',
                            backdrop: `
                            rgba(0,0,123,0.4)
                            left top
                            no-repeat`
                        })
                    })
                })
}

function isInCart(id) {
    if(sessionStorage.getItem('cart')) {
        let storage = sessionStorage.getItem('cart').split(',')

        if(storage.includes(id.toString())) {
            return 'In cart'
        } else {
            return 'Add to cart'
        }
    } else {
        sessionStorage.setItem('cart', '')
        return 'Add to cart'
    }
}

function populateCartBadge() {
    let storage = sessionStorage.getItem('cart').split(',')
    
    let artInCart = articles.filter(art => storage.includes(art.id.toString()))

    let badgeCart = document.querySelector('#badge-cart')
    badgeCart.innerHTML = artInCart.length
}

function trysweetalert(){
    if (document.querySelector(".testsweet")) {
        let btnsweet = document.querySelector(".testsweet")
        btnsweet.addEventListener("click", function(){
            Swal.fire({
                title: 'try sweet alert!',
                width: 500,
                padding: '3em',
                background: '#fff url("https://sweetalert2.github.io/images/trees.png")',
                backdrop: `
                    rgba(0,0,123,0.4)
                    url("https://sweetalert2.github.io/images/nyan-cat.gif")
                    left top
                    no-repeat
                `
                })
            
        })
    }
}

function updateCart() {
    
    let articleId = this.getAttribute('shop-id')

    let storage = sessionStorage.getItem('cart').split(',')

    if(storage.includes(articleId.toString())) {
        storage = storage.filter(el => el != articleId)
        this.innerHTML = 'Add to cart'

    } else {
        storage.push(articleId)
        this.innerHTML = 'In cart'
    }


    sessionStorage.setItem('cart', storage)

    populateCartBadge()

    console.log(sessionStorage.getItem('cart').split(','))
}


populatelastItems('Smartphones', 'smartphone-cards', 'more-mobile-phones')
populatelastItems('Tablets', 'tablet-cards', 'more-tablets')
populatelastItems('Accessories', 'accessories-card', 'more-accessories')
populateCartBadge()
trysweetalert()

});

    
       