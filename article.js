const toggler = document.querySelector('.navbar-toggler')
const navbarCollapse = document.querySelector('.navbar-collapse')

toggler.addEventListener('click' , function() {
    toggler.classList.toggle('fa-rotate-180')
    navbarCollapse.classList.toggle('bg-light')
    toggler.classList.toggle('active')
})

document.addEventListener('scroll' , function() {
    const navbar = document.querySelector('.navbar')
    if(window.scrollY > 100) {
        navbar.classList.remove('navbar-light', 'bg-light')
        navbar.classList.add('navbar-dark', 'bg-dark')
        navbarCollapse.classList.add('bg-dark')
    } else {
        navbar.classList.add('navbar-light', 'bg-light')
        navbar.classList.remove('navbar-dark', 'bg-dark')
        navbarCollapse.classList.remove('bg-dark')
    }
})


fetch('./article.json').then((data) => data.json()).then((articles) => {


let queryString = window.location.search //Recupero parametri eventualmente trasmessi
let urlParams = new URLSearchParams(queryString);
let cat = urlParams.get('cat')
let categoria = (cat) ? cat : /.*/gm //Parametro GET o regEx jolly


function populateCartBadge() {
    let storage = sessionStorage.getItem('cart').split(',')
    
    let artInCart = articles.filter(art => storage.includes(art.id.toString()))

    let badgeCart = document.querySelector('#badge-cart')
    badgeCart.innerHTML = artInCart.length
}

function populateAds(articles) {

    const adsWrapper = document.querySelector('#ads-wrapper')

    adsWrapper.innerHTML = ''

    articles.forEach((article , i) => {
        let card = document.createElement('div')

        card.classList.add('col-12', 'col-md-4', 'mb-5')

        function isInCart(id) {
            if(sessionStorage.getItem('cart')) {
                let storage = sessionStorage.getItem('cart').split(',')

                if(storage.includes(id.toString())) {
                    return 'In cart'
                } else {
                    return 'Add to cart'
                }
            } else {
                sessionStorage.setItem('cart', '')
                return 'Add to cart'
            }
        }

        card.innerHTML = 
        `
        <div data-aos="zoom-in" class="card bg-card d-flex align-items-center mb-5">
            <img src="${article.img}" class="card-img-top img-fluid" alt="...">
            <button ad-id="${article.id}" class="btn btn-card">Quick view</button>
                <div class="card-body p-0 d-flex justify-content-center align-items-center flex-column">
                    <h5 class="card-title fw-bold my-2">${article.name}</h5>
                    <a shop-id="${article.id}" class="btn btn-shop">${isInCart(article.id)}</a>
                    <p class="card-text text-center">${article.price.toFixed(2)} €</p>
                </div>
        </div>
        `
        adsWrapper.appendChild(card)
    })

        let btnShop = document.querySelectorAll('.btn-shop')
        btnShop.forEach(btn => {
            
            btn.addEventListener('click', function() {    
                let articleId = btn.getAttribute('shop-id')

                let storage = sessionStorage.getItem('cart').split(',')

                if(storage.includes(articleId.toString())) {
                    storage = storage.filter(el => el != articleId)
                    btn.innerHTML = 'Add to cart'

                } else {
                    storage.push(articleId)
                    btn.innerHTML = 'In cart'
                }


                sessionStorage.setItem('cart', storage)

                populateCartBadge()

                console.log(sessionStorage.getItem('cart').split(','))
            })
        })

        let btnCard = document.querySelectorAll('.btn-card')
        btnCard.forEach(btn => {
            let articleId = btn.getAttribute('ad-id')
            let article = articles.filter(art => art.id == articleId)
            article = article[0]
            btn.addEventListener('click', function() {
                Swal.fire({
                    html:
                    `
                <div class="row justify-content-around">
                    <div class="col-12 col-md-6">
                            <img src="${article.img}" class="card-img-top img-fluid" alt="...">
                    </div>
                        
                    <div class="col-12 col-md-6">
                        <h4>${article.name}</h4>
                        <h5>${article.brand}</h5>
                        <p>${article.price} €</p>
                        <p class="text-start">${article.description}</p>
                    </div>
                </div>
                    `,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText:
                        '<i class="fa fa-thumbs-up"></i> Great!',
                    confirmButtonAriaLabel: 'Thumbs up, great!',
                    cancelButtonText:
                        '<i class="fa fa-thumbs-down"></i>',
                    cancelButtonAriaLabel: 'Thumbs down',
                    background: '#fff',
                    backdrop: `
                        rgba(0,0,123,0.4)
                        left top
                        no-repeat`
                    })
            })
        })
    
}

function populateCategoryRadio() {
    let categories = Array.from(new Set(articles.map((article) => article.category)));
    let wrapperCategoryRadio = document.querySelector(
        '#wrapper-category-radio'
    );

    categories.forEach((category, i) => {
        let input = document.createElement('div');

        input.classList.add('form-check');

        input.innerHTML = 
        ` 
        <input class="form-check-input filter-category" type="radio" name="category-filter" id="flexRadioDefault${i}"data-filter="${category}">
        <label class="form-check-label" for="flexRadioDefault${i}">${category}</label>
        `

        wrapperCategoryRadio.appendChild(input);
    });

}

function populateBrandSelect() {
    let brands = Array.from(new Set(articles.map(article => article.brand)))
    let wrapper = document.querySelector('#category-select')

    brands.forEach(brand => {
        let option = document.createElement('option')

        option.innerHTML = `${brand}`
        option.value = `${brand}`
        wrapper.appendChild(option)
    })
}

function filterByCategoryRadio(filter) {
    if(filter == 'all') {
        filter = /.*/
    }

    filtered = articles.filter(article => article.category.match(filter))
    populateAds(filtered)
}

function filterByBrandSelect() {
    let input = document.querySelector('#category-select')

    input.addEventListener('change', function(){

        if(input.value === 'all'){
            populateAds(articles)
        } else {
            let filtered = articles.filter(article => article.brand === input.value)

            populateAds(filtered)
        }
    })
}

function populatePriceFilter() {
    let minInput = document.querySelector('#min-price-filter')
    let minLabel = document.querySelector('#min-price-label')

    let maxInput = document.querySelector('#max-price-filter')
    let maxLabel = document.querySelector('#max-price-label')

    let max = articles.map(article => article.price).sort((a , b) => b - a)[0]

    // inizializzo il massimo e l'attributo max
    maxLabel.innerHTML = `${Math.ceil(max)} €`
    minInput.max = max
    maxInput.max = max
    maxInput.value = max

    minInput.addEventListener('input' , function(e) { 
        if((Number(maxInput.value) - 65) <= Number(minInput.value)){
            e.preventDefault()
            minInput.value = Number(maxInput.value) - 65
        }
        minLabel.innerHTML = `${minInput.value} €`
    })

    maxInput.addEventListener('input' , function(e) {
        // maxInput.min = Number(minInput.value) + 10
        if((Number(maxInput.value) - 65) <= Number(minInput.value) ){
            e.preventDefault()
            maxInput.value = Number(minInput.value) + 65
        }
        maxLabel.innerHTML = `${maxInput.value} €`
    })

}

function filterByPrice() {
    let minInput = document.querySelector('#min-price-filter')
    let maxInput = document.querySelector('#max-price-filter')

    function filterAds(){
        let filtered = articles.filter(article => Number(article.price) > Number(minInput.value) && Number(article.price) <= Number(maxInput.value)).sort((a, b) => a.price - b.price);

        populateAds(filtered)
    }

    minInput.addEventListener('change' , filterAds)

    maxInput.addEventListener('change' , filterAds)
}

function filterBySearch(){
    let input = document.querySelector('#search-input')
    
    input.addEventListener('input', function(){

        let filtered = articles.filter(article => article.name.toLowerCase().includes(input.value.toLowerCase()))

        populateAds(filtered) 

    })
}

    populateAds(articles)
    populateCategoryRadio()

    let radios = document.querySelectorAll('.filter-category')
    radios.forEach(radio => {
        radio.addEventListener('input' , function(){filterByCategoryRadio(radio.getAttribute('data-filter'))})
    })

    populateBrandSelect()
    populatePriceFilter()
    populateCartBadge()

    filterBySearch()
    filterByCategoryRadio(categoria)
    filterByBrandSelect()
    filterByPrice()

});