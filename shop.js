const toggler = document.querySelector('.navbar-toggler')
const navbarCollapse = document.querySelector('.navbar-collapse')

toggler.addEventListener('click' , function() {
    toggler.classList.toggle('fa-rotate-180')
    navbarCollapse.classList.toggle('bg-light')
    toggler.classList.toggle('active')
})

document.addEventListener('scroll' , function() {
    const navbar = document.querySelector('.navbar')
    if(window.scrollY > 100) {
        navbar.classList.remove('navbar-light', 'bg-light')
        navbar.classList.add('navbar-dark', 'bg-dark')
        navbarCollapse.classList.add('bg-dark')
    } else {
        navbar.classList.add('navbar-light', 'bg-light')
        navbar.classList.remove('navbar-dark', 'bg-dark')
        navbarCollapse.classList.remove('bg-dark')
    }
})


fetch('./article.json').then((data) => data.json()).then((articles) => {

function populateCartBadge() {
        if(sessionStorage.getItem('cart') != null) {
            let storage = sessionStorage.getItem('cart').split(',')
            
            let artInCart = articles.filter(art => storage.includes(art.id.toString()))
        
            let badgeCart = document.querySelector('#badge-cart')
            badgeCart.innerHTML = artInCart.length
        } else {
            let storage = sessionStorage.setItem('cart', '')
            let badgeCart = document.querySelector('#badge-cart')
            badgeCart.innerHTML = 0
        }
}

function populateCardProduct() {

    let storage = sessionStorage.getItem('cart').split(',')
        
    let artInCart = articles.filter(art => storage.includes(art.id.toString()))

    if(artInCart.length == ['']) {
        let cartWrapper = document.querySelector('#cart-wrapper')
    
        let card = document.createElement('div')
    
        card.classList.add('row', 'my-4')
    
        card.innerHTML = 
        `
        <div class="col-12 text-center">
        <h5 class="text-secondary my-4">The cart is empty</h5>
        <i class="fas fa-shopping-cart fs-1"></i>
        </div>
        `
        cartWrapper.appendChild(card)
    } else {
        artInCart.forEach(art => {
            let cartWrapper = document.querySelector('#cart-wrapper')
    
            let card = document.createElement('div')
    
            card.classList.add('row', 'cart-row', 'my-4', 'justify-content-around')
    
            card.innerHTML = 
            `
            <div class="col-6 col-md-2">
                <img src="${art.img}" class="border img-fluid">
            </div>
            <div class="col-6 col-md-3">
                <h6>${art.name}</h6>
                <h6 class="cart-price text-secondary">${art.price.toFixed(2)}€</h6>
                <h6 class="text-secondary">Product: ${art.category}</h6>
                <h6 class="text-secondary">Brand: ${art.brand}</h6>
    
                <input class="form-control cart-quantity-input my-4" value="1" type="number">
            </div>
    
            <div class="col-3 col-md-2 px-0 text-center">
                <h5 class="cart-price fw-bold my-4">${art.price.toFixed(2)}€</h5>
            </div>
    
            <div class="col-3 col-md-2 px-0 text-center">
                <i item-id='${art.id}' id='remove-item' class="fas fa-times my-4"></i>
            </div>
            `
            cartWrapper.appendChild(card)
        })
    }
}

function removeItemFromCart() {

    let removeCartItemButtons = document.querySelectorAll('#remove-item')
    removeCartItemButtons.forEach(btn => {
        btn.addEventListener('click', function() {
            let articleId = btn.getAttribute('item-id')

            let storage = sessionStorage.getItem('cart').split(',')

            if(storage.includes(articleId)) {
                storage = storage.filter(el => el != articleId)
                btn.parentElement.parentElement.remove()
            }
            
            sessionStorage.setItem('cart', storage)
            populateCartBadge()
            updateCartTotal()

            let artInCart = articles.filter(art => storage.includes(art.id.toString()))
            if(artInCart.length == ['']) {
                let cartWrapper = document.querySelector('#cart-wrapper')
            
                let card = document.createElement('div')
            
                card.classList.add('row', 'my-4')
            
                card.innerHTML = 
                `
                <div class="col-12 text-center">
                <h5 class="text-secondary my-4">The cart is empty</h5>
                <i class="fas fa-shopping-cart fs-1"></i>
                </div>
                `
                cartWrapper.appendChild(card)
            }

            console.log(sessionStorage.getItem('cart').split(','))
        })
    })
}

function quantityChanged() {
    let quantityInputs = document.querySelectorAll('.cart-quantity-input')
    quantityInputs.forEach(input => {
        input.addEventListener('change', function() {
            if (isNaN(input.value) || input.value <= 0) {
                input.value = 1
            }
            updateCartTotal()
        })
    })
}

function updateCartTotal() {
    var cartRows = document.getElementsByClassName('cart-row')
    var total = 0
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i]
        var priceElement = cartRow.getElementsByClassName('cart-price')[0]
        var quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0]
        var price = parseFloat(priceElement.innerText.replace('€', ''))
        var quantity = quantityElement.value
        total = total + (price * quantity)
    }
    total = Math.round(total * 100) / 100
    document.querySelectorAll('.cart-total-price').forEach(tot => {
        tot.innerHTML = '€' + total.toFixed(2)
    })
}

function pressCheckout(){
    if (document.querySelector("#checkout")) {
        let btnsweet = document.querySelector("#checkout")
        btnsweet.addEventListener("click", function(){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: `We can't accept online orders right now.`,
                footer: 'Please contact us to complete your purchase.',
                confirmButtonText: 'Got it'
              })
            
        })
    }
}

populateCartBadge()
populateCardProduct()
removeItemFromCart()
quantityChanged()
updateCartTotal()
pressCheckout()

});
